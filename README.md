# Arno

<p align="center" dir="auto">
  <a target="_blank" rel="noopener noreferrer" href="https://user-images.githubusercontent.com/93531354/155890601-5919d2fe-81be-486e-91d1-d93f8ef734cb.png"><img alt="Arno" src="https://user-images.githubusercontent.com/93531354/155890601-5919d2fe-81be-486e-91d1-d93f8ef734cb.png" height="240" style="max-width: 100%;"></a>
  <br>
  <strong>Arno - An automation tool to install the most popular tools for bug bounty or pentesting</strong> 
  <br><br>
  <strong>Recode The Copyright Is Not Make You A Coder</strong>
</p>  
  
<div>
  <p align="center" dir="auto">
  <a href="https://www.instagram.com/prohacker77_/" target="_blank"><img src="https://img.shields.io/badge/-Instagram-%23E4405F?style=for-the-badge&logo=instagram&logoColor=39ff14&logoColor=white&color=black" target="_blank"></a>
  <a href="https://discord.gg/Z2C2CyVZFU" target="_blank"><img src="https://img.shields.io/badge/-Discord-7289DA?style=for-the-badge&logo=discord&logoColor=39ff14&logoColor=white&color=black" target="_blank"></a>
  <a href="https://www.linkedin.com/in/caique-barreto-7809b2217/" target="_blank"><img src="https://img.shields.io/badge/-LinkdIn-%230077B5?style=for-the-badge&logo=linkedin&logoColor=39ff14&logoColor=white&color=black" target="_blank"></a>
  <a href="mailto:caique.hbarreto@gmail.com" target="_blank"><img src="https://img.shields.io/badge/-Gmail-%23333?style=for-the-badge&logo=gmail&logoColor=39ff14&logoColor=white&color=black" target="_blank"></a>
  <a href="https://t.me/PeakyBlindersW" target="_blank"><img src="https://img.shields.io/badge/Telegram-2CA5E0?style=for-the-badge&logo=telegram&logoColor=39ff14&logoColor=white&color=black" target="_blank"></a>
  </p>
</div>

## An automation tool to install the most popular tools for bug bounty or pentesting! This will save you 90% of your time when setting up your machine to work.
It already configures all the tools for you to work, you won't need to configure it manually.

> Status: Developing ⚠️


### Tool updates every linux directory and all dependencies needed to work
   - tor, argparse, pyrit, requests, proxychains4, aptitude, Seclists, synaptic, brave, hashcat, docker.io, exploitdb-papers, exploitdb-bin-sploits etc...


### Script to install the most popular tools used when looking for vulnerabilities for a Bug bounty or Pentest bounty program.

## Operating Systems Supported
| OS         | Supported | Easy Install  | Tested        | 
|------------|-----------|---------------|---------------|
| Ubuntu     |    No     | Yes           | Ubuntu 20.04  |
| Kali       |    Yes    | Yes           | Kali 2021.4   |
| Debian     |    Yes    | Yes           | Debian 10     |
| Arch Linux |    Yes    | No            | Yes           |

<p align="center" dir="auto"><strong>Tools used - You must need to install these tools to use this script</strong><br></p>


- [x]  Amass
- [x]  Anon-SMS
- [x]  Asnlookup
- [x]  A2SV
- [x]  Arjun
- [x]  aiodnsbrute
- [x]  anew
- [x]  Anonsurf
- [x]  anti-burl
- [x]  aquatone
- [x]  assetfinder
- [x]  Brave
- [x]  Bluto
- [x]  Corsy
- [x]  CrackMapExec
- [x]  cent
- [x]  cf-check
- [x]  chaos-client
- [x]  cngo
- [x]  commix
- [x]  concurl
- [x]  crlfuzz
- [x]  crobat
- [x]  ctfr
- [x]  DNSCewl
- [x]  Docker
- [x]  dalfox
- [x]  dirdar
- [x]  Dirsearch
- [x]  dnscewl
- [x]  dnsgen
- [x]  dnsrecon
- [x]  dnsvalidator
- [x]  dnsx
- [x]  Droopescan
- [x]  ERLPopper
- [x]  exclude-cdn
- [x]  feroxbuster
- [x]  fff
- [x]  ffuf
- [x]  ffuz
- [x]  findomain
- [x]  Feroxbuster
- [x]  Gf-Patterns
- [x]  Go
- [x]  Gxss
- [x]  gau
- [x]  gauplus
- [x]  getJS
- [x]  gf
- [x]  gfpatterns
- [x]  github-endpoints
- [x]  github-subdomains
- [x]  Gittools
- [x]  Git-dumper
- [x]  gobuster
- [x]  google-chrome
- [x]  gorgo
- [x]  gospider
- [x]  gowitness
- [x]  gron
- [x]  gxss
- [x]  hakrawler
- [x]  http2smugl
- [x]  httprobe
- [x]  httpx
- [x]  Interlace
- [x]  interactsh-client
- [x]  Infoga
- [x]  jaeles
- [x]  Joomscan
- [x]  kiterunner
- [x]  kxss
- [x]  Knock
- [x]  LinkFinder
- [x]  leaky-paths
- [x]  masscan
- [x]  massdns
- [x]  Metagoofil
- [x]  medusa
- [x]  meg
- [x]  naabu
- [x]  Ngrok
- [x]  nmap
- [x]  notify
- [x]  nuclei
- [x]  OpenRedireX
- [x]  Osintgram
- [x]  ParamSpider
- [x]  Parsero
- [x]  phantomjs
- [x]  Phoneinfoga
- [x]  Pyrit
- [x]  proxychains-ng
- [x]  Pwndb
- [x]  puredns
- [x]  qsreplace
- [x]  RustScan
- [x]  responder
- [x]  SecLists
- [x]  Seeker
- [x]  s3scanner
- [x]  scrying
- [x]  shuffledns
- [x]  Sslyze
- [x]  Sslscan
- [x]  sn0int
- [x]  soxy
- [x]  sqlmap
- [x]  subfinder
- [x]  sublist3r
- [x]  Saycheese
- [x]  Sayhello
- [x]  Sherlok
- [x]  subjack
- [x]  subjs
- [x]  SocialFish
- [x]  Takeover
- [x]  testssl
- [x]  TheHarvester
- [x]  Twitter-info
- [x]  Twintproject
- [x]  The-endorser
- [x]  tlscout
- [x]  ufw
- [x]  Unfurl
- [x]  unimap
- [x]  urldedupe
- [x]  uro
- [x]  wafw00f
- [x]  Wfuzz
- [x]  WPSeku
- [x]  Wafninja
- [x]  waybackurls
- [x]  webscreenshot
- [x]  Whatweb
- [x]  whois
- [x]  wpscan
- [x]  Xsstrike
- [x]  Zphisher


### Full installation
#### cURL, Recommends installing in root mode

```sh
curl -sL https://github.com/DonatoReis/arno/raw/main/install.sh | sudo bash

```

#### **Installation mode with git clone**
```sh
git clone https://github.com/DonatoReis/arno
sudo arno/install.sh
```


Warning: This code was originally created for personal use, it generates a substantial amount of traffic, please use with caution.
